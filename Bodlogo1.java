package yvts2;
import java.util.Scanner;
import java.util.stream.Stream;


@FunctionalInterface
interface Calculate{
    int calculateMonths(double x, double y, double z);
}


public class Bodlogo1 {

    public static void main(String[] args) {
        double k, p, s;
        Scanner input = new Scanner(System.in);
        k = input.nextDouble();
        p = input.nextDouble();
        s = input.nextDouble();

        Calculate calculate = (double anh, double nemelt, double niit) -> {
            return Stream.iterate(anh, amount -> amount * (1 + (nemelt / 100)))
                    .takeWhile(amount -> amount < niit)
                    .toArray()
                    .length;
        };
        int months = calculate.calculateMonths(k,p,s);
    }
}
