package yvts2;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Bodlogo2 {
    public static void main(String[] args) {
        List<Book> books = readBooks("src/test.txt");

        int minPrice = books.stream()
                .mapToInt(Book::getUne)
                .min()
                .orElse(0);

        List<Book> books1 = books.stream()
                .filter(book -> book.getUne() == minPrice)
                .collect(Collectors.toList());

        System.out.println("Hamgiin hymd unetei nomnuud:");
        books1.forEach(book -> System.out.println(book.getNer()));

        int maxPrice = books.stream()
                .mapToInt(Book::getUne)
                .max()
                .orElse(0);

        List<Book> books2 = books.stream()
                .filter(book -> book.getUne() == maxPrice)
                .collect(Collectors.toList());

        int minHuudasniiToo = books2.stream()
                .mapToInt(Book::getHuudasniiToo)
                .min()
                .orElse(0);

        List<Book> books3 = books2.stream()
                .filter(book -> book.getHuudasniiToo() == minHuudasniiToo)
                .collect(Collectors.toList());

        System.out.println("Hamgiin unetei ba hamgiin nimgen nom ni:");
        books3.forEach(book -> System.out.println(book.getNer()));
    }


    private static List<Book> readBooks(String filename) {
        try {
            return Files.lines(Paths.get(filename))
                    .map(line -> line.split("\\W"))
                    .filter(parts -> parts.length == 4)
                    .map(parts -> new Book(parts[0], parts[1], Integer.parseInt(parts[2]), Integer.parseInt(parts[3])))
                    .toList();
        } catch (IOException e) {
            System.err.println("Error: " + e.getMessage());
            return List.of();
        }
    }
}


